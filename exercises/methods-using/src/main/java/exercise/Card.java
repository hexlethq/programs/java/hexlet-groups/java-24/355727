package exercise;

class Card {
    public static void printHiddenCard(String cardNumber, int starsCount) {
        // BEGIN
        int length = cardNumber.length();
        System.out.println("*".repeat(starsCount) + cardNumber.substring(length - 4));
        // END
    }
}
