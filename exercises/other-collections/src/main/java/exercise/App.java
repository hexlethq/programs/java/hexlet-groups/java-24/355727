package exercise;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

// BEGIN
public class App {
    public static LinkedHashMap genDiff(Map<String, Object> firstMap, Map<String, Object> secondMap) {
        var finishMap = new LinkedHashMap<String, String>();
        firstMap.entrySet()
                .forEach(maps -> {
            String key = maps.getKey();
            var value = maps.getValue();
            if (secondMap.containsKey(key)) {
                if (value.equals(secondMap.get(key))) {
                    finishMap.put(key, "unchanged");
                } else {
                    finishMap.put(key, "changed");
                }
            } else {
                finishMap.put(key, "deleted");
            }
        });

        secondMap
                .keySet()
                .stream()
                .filter(k -> !firstMap.containsKey(k)).forEach(k -> finishMap.put(k, "added"));

        return finishMap;
    }
}

//END
