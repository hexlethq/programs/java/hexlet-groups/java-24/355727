package exercise;

import java.util.Arrays;

// BEGIN
public class Kennel {
    private static String[][] puppies = new String[0][2];

    public static void addPuppy(String[] puppy) {
        puppies = Arrays.copyOf(puppies, puppies.length + 1);
        puppies[puppies.length - 1] = puppy;
    }

    public static void addSomePuppies(String[][] somePuppies) {
        int count = getPuppyCount();
        puppies = Arrays.copyOf(puppies, count + somePuppies.length);
        int k = 0;
        int i = count;
        while (i < puppies.length) {
            puppies[i] = somePuppies[k];
            k++;
            i++;
        }
    }

    public static int getPuppyCount() {
        return puppies == null ? 0 : puppies.length;
    }

    public static boolean isContainPuppy(String name) {
        return Arrays.stream(puppies).anyMatch(puppy -> name.equals(puppy[0]));
    }

    public static String[][] getAllPuppies() {
        return puppies;
    }

    public static String[] getNamesByBreed(String breed) {
        int size = (int) Arrays.stream(puppies).filter(puppy -> puppy[1].equals(breed)).count();
        String[] names = new String[size];
        int i = 0;
        int f = 0;
        int puppiesLength = puppies.length;
        while (f < puppiesLength) {
            String[] puppy = puppies[f];
            if (puppy[1].equals(breed)) {
                names[i] = puppy[0];
                i++;
            }
            f++;
        }
        return names;
    }

    public static void resetKennel() {
        puppies = new String[0][2];
    }

}
// END
