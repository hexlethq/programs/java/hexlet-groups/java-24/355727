package exercise;

import java.util.Arrays;

class Point {
    // BEGIN
    public static int[] makePoint(int x, int y) {
        return new int[]{x, y};
    }

    public static int getX(int[] point) {
        return point[0];
    }

    public static int getY(int[] point) {
        return point[1];
    }

    public static String pointToString(int[] point) {
        return Arrays.toString(point).replace('[', '(').replace(']', ')');
    }

    public static int getQuadrant(int[] point) {
        if (getX(point) == 0 || getY(point) == 0) {
            return 0;
        }

        if (getX(point) > 0) {
            return getY(point) > 0 ? 1 : 4;
        } else {
            return getY(point) > 0 ? 2 : 3;
        }

    }

    public static int[] getSymmetricalPointByX(int[] point) {

        return makePoint(getX(point), -getY(point));
    }

    // END
}
