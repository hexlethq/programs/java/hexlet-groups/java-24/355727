package exercise;
import exercise.connections.Connection;
import exercise.connections.Disconnected;

// BEGIN
public class TcpConnection {

    private Connection connectionState;
    String ip;
    int port;

    public TcpConnection(String ip, int port) {
        this.connectionState = new Disconnected(this);
        this.ip = ip;
        this.port = port;
    }

    public String getCurrentState() {
        return this.connectionState.getCurrentState();
    }

    public void setCurrentState(Connection currentState) {
        this.connectionState = currentState;
    }

    public void write(String data) {
        this.connectionState.write(data);
    }

    public void connect() {
        this.connectionState.connect();
    }

    public void disconnect() {
        this.connectionState.disconnect();
    }

}

// END
