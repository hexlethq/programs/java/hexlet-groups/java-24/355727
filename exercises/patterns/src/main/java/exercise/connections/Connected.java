package exercise.connections;

import exercise.TcpConnection;

// BEGIN
public class Connected implements Connection {
    TcpConnection tcpConnection;

    Connected(TcpConnection tcp) {
        this.tcpConnection = tcp;
    }

    @Override
    public void connect() {
        System.out.println("Error: connection connected");
    }

    @Override
    public void disconnect() {
        this.tcpConnection.setCurrentState(new Disconnected(tcpConnection));
        System.out.println("disconnected");
    }

    @Override
    public void write(String data) {
        System.out.println(data);
    }

    @Override
    public String getCurrentState() {
        return "connected";
    }
}
// END
