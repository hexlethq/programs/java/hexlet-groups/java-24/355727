package exercise.controllers;

import io.javalin.http.Context;
import io.javalin.apibuilder.CrudHandler;
import io.ebean.DB;

import exercise.domain.User;
import exercise.domain.query.QUser;

import org.apache.commons.validator.routines.EmailValidator;
import org.apache.commons.lang3.StringUtils;

public class UserController implements CrudHandler {

    public void getAll(Context ctx) {
        // BEGIN
        var users = new QUser().findList();
        var usersJson = DB.json().toJson(users);
        ctx.json(usersJson);
        // END
    };

    public void getOne(Context ctx, String id) {

        // BEGIN
        var user = new QUser().id
                .equalTo(Long.parseLong(id))
                .findOne();
        var userJson = DB.json().toJson(user);
        ctx.json(userJson);
        // END
    };

    public void create(Context ctx) {

        // BEGIN
        var user = ctx.bodyValidator(User.class)
                .check(value -> value.getFirstName() != null, "The \"Name\" field must be filled in. ")
                .check(value -> value.getLastName() != null, "The \"Last Name\" field must be filled in. ")
                .check(value -> EmailValidator.getInstance().isValid(value.getEmail()), "Invalid email entered.")
                .check(value -> StringUtils.isNumeric(value.getPassword()), "Password must contain only numbers.")
                .check(value -> value.getPassword().length() >= 3, "Password must be 3 or more characters.")
                .get();

        user.save();

        // END

        // END
    };

    public void update(Context ctx, String id) {
        // BEGIN
        var body = ctx.body();
        var user = DB.json().toBean(User.class, body);
        user.setId(id);
        user.update();

        // END
    };

    public void delete(Context ctx, String id) {
        // BEGIN
        var user = new User();
        user.setId(id);
        user.delete();
        // END
    };
}
