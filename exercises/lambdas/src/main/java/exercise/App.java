package exercise;

import java.util.Arrays;
import java.util.stream.Stream;

// BEGIN
public class App {
    public static String[][] enlargeArrayImage(String[][] image) {
        var result = Arrays.stream(image)
                .flatMap(element -> Stream.of(element, element))
                .map(element -> Arrays.stream(element)
                        .flatMap(enlarge -> Stream.of(enlarge, enlarge)))
                .map(element -> element.toArray(String[]::new))
                .toArray(String[][]::new);
        return result;
    }
}

// END
