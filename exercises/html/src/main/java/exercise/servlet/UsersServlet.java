package exercise.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.text.MessageFormat;
import java.util.List;
import java.util.Map;

import java.nio.file.Paths;
import java.nio.file.Files;
import java.util.Objects;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.commons.lang3.ArrayUtils;

public class UsersServlet extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws IOException, ServletException {

        String pathInfo = request.getPathInfo();

        if (pathInfo == null) {
            showUsers(request, response);
            return;
        }

        String[] pathParts = pathInfo.split("/");
        String id = ArrayUtils.get(pathParts, 1, "");

        showUser(request, response, id);
    }

    private List getUsers() throws JsonProcessingException, IOException {
        // BEGIN
        var map = new ObjectMapper();
        var path = "src/main/resources/users.json";
        return map.readValue(new String(Files.readAllBytes(Paths.get(path))), List.class);
        // END
    }

    private void showUsers(HttpServletRequest request,
                           HttpServletResponse response)
            throws IOException {

        // BEGIN
        response.setContentType("text/html;charset=UTF-8");
        List<Map<String, String>> users = getUsers();
        var out = response.getWriter();
        var stringBuilder = new StringBuilder();
        stringBuilder.append("<table>");
        users.forEach(usersList -> {
            var id = usersList.get("id");
            var fullName = MessageFormat.format("{0} {1}", usersList.get("firstName"), usersList.get("lastName"));
            stringBuilder.append("<tr><td>")
                    .append(id)
                    .append("</td>")
                    .append("<td><a href=\"/users/")
                    .append(id)
                    .append("\">")
                    .append(fullName)
                    .append("</a></td></tr>");
        });
        stringBuilder.append("</table>");
        out.print(stringBuilder);
        // END
    }

    private void showUser(HttpServletRequest request,
                          HttpServletResponse response,
                          String id)
            throws IOException {

        // BEGIN
        var user = new StringBuilder();
        List<Map<String, String>> users = getUsers();
        if (users.size() == 0) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND, "Not found");
            return;
        }
        for (Map<String, String> stringStringMap : users) {
            if (Objects.equals(stringStringMap.get("id"), id)) {
                user.append("<table>");
                var firstName = stringStringMap.get("firstName");
                var lastName = stringStringMap.get("lastName");
                var email = stringStringMap.get("email");

                user.append("<tr><td>")
                        .append(id)
                        .append("</td><td>")
                        .append(firstName)
                        .append("</td><td>")
                        .append(lastName)
                        .append("</td><td>")
                        .append(email)
                        .append("</td>")
                        .append("</table>");
                var print = response.getWriter();
                print.println(user);
                return;
            }
        }
        response.sendError(HttpServletResponse.SC_NOT_FOUND, "Not found");
        // END
    }
}
