// BEGIN
package exercise;

import exercise.geometry.Point;
import exercise.geometry.Segment;

public class App {

    public static double[] getMidpointOfSegment(double[][] segment) {
        return new double[]{getMidpoint(segment, 'X'), getMidpoint(segment, 'Y')};
    }

    public static double getMidpoint(double[][] segment, char coordinate) {
        switch (coordinate) {
            case 'X' -> {
                return (Point.getX(Segment.getBeginPoint(segment)) + Point.getX(Segment.getEndPoint(segment))) / 2;
            }
            case 'Y' -> {
                return (Point.getY(Segment.getBeginPoint(segment)) + Point.getY(Segment.getEndPoint(segment))) / 2;

            }
            default -> throw new IllegalStateException("Unexpected value: " + coordinate);
        }

    }

    public static double[][] reverse(double[][] segment) {
        var reversedSegment = new double[2][2];
        reversedSegment[0] = Segment.getEndPoint(segment);
        reversedSegment[1] = Segment.getBeginPoint(segment);
        return reversedSegment;
    }

}

// END
