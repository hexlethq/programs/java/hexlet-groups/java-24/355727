// BEGIN
package exercise.geometry;

import static exercise.geometry.Point.getX;
import static exercise.geometry.Point.getY;

public class Segment {
    public static double[][] makeSegment(double[] a, double[] b) {
        var segment = new double[2][2];
        segment[0][0] = getX(a);
        segment[0][1] = getY(a);
        segment[1][0] = getX(b);
        segment[1][1] = getY(b);
        return segment;
    }

    public static double[] getBeginPoint(double[][] segment) {
        return segment[0];
    }

    public static double[] getEndPoint(double[][] segment) {
        return segment[1];
    }

}
// END
