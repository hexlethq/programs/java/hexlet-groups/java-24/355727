package exercise;


import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

// BEGIN
public class App {
    public static HashMap<String, Integer> getWordCount(String sentence) {
        var wordCount = new HashMap<String, Integer>();
        if ("".equals(sentence)) {
            return wordCount;
        }
        Arrays.stream(sentence.split(" "))
                .forEachOrdered(word -> wordCount.put(word, !wordCount.containsKey(word) ? 1 : wordCount.get(word) + 1));
        return wordCount;
    }

    public static String toString(Map<String, Integer> inputMap) {
        if (inputMap.isEmpty()) {
            return "{}";
        }
        var builder = new StringBuilder();
        builder.append("{\n");
        inputMap.forEach((key, value) -> {
            builder.append("  ");
            builder.append(key);
            builder.append(": ");
            builder.append(String.valueOf(value));
            builder.append("\n");
        });
        builder.append("}");
        return builder.toString();
    }
}

//END
