package exercise;

class App {
    public static void numbers() {
        // BEGIN
        int y = 100 % 3;
        int x = 8 / 2;
        System.out.println(x + y);
        // END
    }

    public static void strings() {
        String language = "Java";
        // BEGIN
        System.out.println(language + " works on JVM");
        // END
    }

    public static void converting() {
        Number soldiersCount = 300;
        String name = "spartans";
        // BEGIN
        System.out.println(soldiersCount + " " + name);
        // END
    }
}
