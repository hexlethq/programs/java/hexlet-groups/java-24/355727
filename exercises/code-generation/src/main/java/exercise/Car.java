package exercise;

import lombok.Value;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

// BEGIN
@Value
// END
class Car {
    int id;
    String brand;
    String model;
    String color;
    User owner;

    public static Car unserialize(String jsonRepresentation) throws IOException {
        var objectMapper = new ObjectMapper();
        return objectMapper.readValue(jsonRepresentation, Car.class);
    }

    // BEGIN
    public String serialize() throws IOException {
        var objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(this);
    }

    // END
}
