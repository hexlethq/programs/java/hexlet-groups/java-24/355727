package exercise;

import java.util.*;

// BEGIN
public class Validator {

    public static List validate(Address address) {
        List<String> nonValidFields = new ArrayList<>();
        Arrays.stream(address.getClass()
                        .getDeclaredFields())
                .filter(field -> field.getAnnotation(NotNull.class) != null)
                .forEach(field -> {
                    field.setAccessible(true);
                    try {
                        if (field.get(address) == null) {
                            nonValidFields.add(field.getName());
                        }
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                });
        return nonValidFields;
    }

    public static Map<String, List<String>> advancedValidate(Address address) {
        Map<String, List<String>> notValidFields = new HashMap<>();
        Arrays.stream(address.getClass().getDeclaredFields())
                .filter(field -> field.getAnnotation(NotNull.class) != null)
                .forEach(field -> {
                    field.setAccessible(true);
                    try {
                        var fieldValue = field.get(address);
                        if (fieldValue == null) {
                            notValidFields.put(field.getName(), Collections.singletonList("can not be null"));
                        } else {
                            if (field.getAnnotation(MinLength.class) != null && fieldValue.toString().length() < field.getAnnotation(MinLength.class).minLength()) {
                                notValidFields.put(field.getName(), Collections.singletonList("length less than " + field.getAnnotation(MinLength.class).minLength()));
                            }
                        }
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                });
        return notValidFields;
    }
}
// END
