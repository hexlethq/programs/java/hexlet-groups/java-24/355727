package exercise;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

// BEGIN
public class App {
    public static List<String> buildAppartmentsList(List<Home> appartments, int numbers) {

        List<Home> toSort = new ArrayList<>();
        {
            int i = 0, appartmentsSize = appartments.size();
            while (i < appartmentsSize) {
                Home appartment = appartments.get(i);
                toSort.add(appartment);
                i++;
            }
        }
        toSort.sort(Comparator.comparing(Home::getArea));
        List<String> list = new ArrayList<>();
        long limit = numbers;
        int i = 0;
        while (i < toSort.size()) {
            Home appartment = toSort.get(i);
            if (limit-- == 0) break;
            String toString = appartment.toString();
            list.add(toString);
            i++;
        }
        return list;

    }
}

// END
