package exercise.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.List;
import java.util.StringJoiner;

import static exercise.Data.getCompanies;

public class CompaniesServlet extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws IOException, ServletException {

        // BEGIN
        var writer = response.getWriter();
        var search = request.getParameter("search");
        List<String> companyList = getCompanies();

        var joiner = new StringJoiner(System.lineSeparator());
        if (search == null || search.equals("")) {
            companyList.forEach(joiner::add);
            writer.println(joiner);
        } else {
            companyList.stream().filter(company -> company.contains(search)).forEach(joiner::add);
            var companyListAfterSearch = joiner.toString();

            writer.println(companyListAfterSearch.isEmpty() ? "Companies not found" : companyListAfterSearch);

        }

        writer.close();

        // END
    }
}
