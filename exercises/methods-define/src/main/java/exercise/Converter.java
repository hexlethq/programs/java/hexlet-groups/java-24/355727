package exercise;

class Converter {
    // BEGIN
    public static int convert(int a, String b) {
        switch (b) {
            case "b":
                a = a * 1024;
                break;
            case "kb":
                a = a / 1024;
                break;
            default:
                a = 0;
                break;
        }
        return a;
    }

    public static void main(String[] args) {

        String test = String.format("10 Kb = %d b", convert(10, "b"));
        System.out.println(test);
    }
    // END
}
