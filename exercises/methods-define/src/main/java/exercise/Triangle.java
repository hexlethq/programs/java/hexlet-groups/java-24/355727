package exercise;

class Triangle {
    // BEGIN
    public static double getSquare(int a, int b, int c) {
        double angle = Math.sin((c * Math.PI) / 180);
        double result = 0.5 * (a * b * angle);
        return result;
    }

    public static void main(String[] args) {
        System.out.println(getSquare(4, 5, 45));
    }
    // END
}
