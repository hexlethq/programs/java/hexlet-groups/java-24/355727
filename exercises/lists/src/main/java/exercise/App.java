package exercise;

import java.util.ArrayList;
import java.util.List;

// BEGIN
public class App {
    public static boolean scrabble(String randomSymbols, String inputWord) {
        var requiredWord = inputWord.toLowerCase();
        List<Character> randomSymbolsAsList = new ArrayList<>(transformToList(randomSymbols));

        int count = 0;
        int i = 0;
        while (i < requiredWord.length()) {
            var charOfWord = requiredWord.charAt(i);
            for (Character s : randomSymbolsAsList) {
                if (charOfWord == s) {
                    randomSymbolsAsList.remove(s);
                    count++;
                    break;
                }
            }
            i++;
        }
        return count == requiredWord.length();
    }

    private static List<Character> transformToList(String inputWord) {
        var wordAsCharArray = inputWord.toLowerCase().toCharArray();
        List<Character> charArrayAsList = new ArrayList<>();
        for (char c : wordAsCharArray) {
            charArrayAsList.add(c);
        }
        return charArrayAsList;
    }
}
//END
