package exercise;

class App {

    public static boolean isBigOdd(int number) {
        // BEGIN
        boolean isBigOddVariable;
        isBigOddVariable = (number % 2 == 1 && number >= 1001) ? true : false;
        // Старый вариант
//        if (number % 2 == 1 && number >= 1001) {
//            isBigOddVariable = true;
//        } else {
//            isBigOddVariable = false;
//        }
        // END
        return isBigOddVariable;
    }

    public static void sayEvenOrNot(int number) {
        // BEGIN
        String s = (number % 2 == 1) ? "no" : "yes";
        System.out.println(s);
        // Старый вариант
//        if (number % 2 == 1) {
//            System.out.println("no");
//        } else {
//            System.out.println("yes");
//        }
        // END
    }

    public static void printPartOfHour(int minutes) {
        // BEGIN
        String r = "";
        switch (minutes / 15) {
            case 0:
                r = "First";
                break;
            case 1:
                r = "Second";
                break;
            case 2:
                r = "Third";
                break;
            case 3:
                r = "Fourth";
                break;
            default:
                r = "Out of range";
                break;
        }
        System.out.println(r);
        // Старый вариант
//        String x="";
//        if (minutes >= 0 && minutes <= 14) {x="First"; }
//        else if (minutes >= 15 && minutes <= 30) {x="Second"; }
//        else if (minutes >= 31 && minutes <= 45) {x="Third"; }
//        else if (minutes >= 46 && minutes <= 59) {x="Fourth"; }
//        System.out.println(x);
        // END
    }
}
