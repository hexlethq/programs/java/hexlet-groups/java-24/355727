package exercise;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

// BEGIN
public class App {
    public static List<Map<String, String>> findWhere(List<Map<String, String>> books, Map<String, String> where) {
        var result = IntStream.range(0, books.size())
                .filter(i -> books.get(i).entrySet().containsAll(where.entrySet()))
                .mapToObj(books::get)
                .collect(Collectors.toList());
        return result;
    }
}

//END
