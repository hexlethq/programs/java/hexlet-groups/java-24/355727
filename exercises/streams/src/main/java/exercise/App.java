package exercise;

import java.util.List;


// BEGIN
public class App {
    public static long getCountOfFreeEmails(List<String> emails) {
        return emails.stream()
                .filter(App::freeDomain)
                .count();
    }

    private static boolean freeDomain(String email) {
        return (email.contains("@gmail") || email.contains("@yandex") || email.contains("@hotmail"));
    }
}
// END
