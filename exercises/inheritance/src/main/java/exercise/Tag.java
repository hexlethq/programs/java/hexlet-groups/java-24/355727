package exercise;

import java.util.Map;
import static java.util.stream.Collectors.joining;

class Tag {

    private final String name;
    private final Map<String, String> attributes;

    Tag(String name, Map<String, String> attributes) {
        this.name = name;
        this.attributes = attributes;
    }

    public String attributesToString() {
        return attributes.keySet()
                .stream()
                .map(key -> {
                    var value = attributes.get(key);
                    return new StringBuilder()
                            .append(" ")
                            .append(key)
                            .append("=\"")
                            .append(value)
                            .append("\"")
                            .toString();
                })
                .collect(joining(""));
    }

    public String getName() {
        return name;
    }
}
