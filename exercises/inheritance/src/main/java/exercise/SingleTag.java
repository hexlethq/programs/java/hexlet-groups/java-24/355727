package exercise;

import java.util.Map;

// BEGIN
public class SingleTag extends Tag {

    SingleTag(String name, Map<String, String> attributes) {
        super(name, attributes);
    }

    @Override
    public String toString() {
        return new StringBuilder()
                .append("<")
                .append(getName())
                .append(attributesToString())
                .append(">")
                .toString();
    }
}
// END
