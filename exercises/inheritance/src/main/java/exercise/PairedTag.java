package exercise;

import java.util.List;
import java.util.Map;

// BEGIN
public class PairedTag extends Tag {

    String text;
    List<Tag> tags;

    public PairedTag(String name, Map<String, String> attributes, String text, List<Tag> tags) {
        super(name, attributes);
        this.text = text;
        this.tags = tags;
    }

    @Override
    public String toString() {
        var stringBuilder = new StringBuilder();

        tags.stream()
                .map(Object::toString)
                .forEach(stringBuilder::append);

        return new StringBuilder().append("<")
                .append(getName())
                .append(attributesToString())
                .append(">")
                .append(text)
                .append(stringBuilder)
                .append("</")
                .append(getName())
                .append(">")
                .toString();
    }
}
// END
