package exercise;

class App {
    // BEGIN
    public static final String NOTATRIANGLE = "Треугольник не существует";
    public static final String EQUILATERAL = "Равносторонний";
    public static final String ISOSCELES = "Равнобедренный";
    public static final String VERSATILE = "Разносторонний";

    public static String getTypeOfTriangle(int a, int b, int c) {
        boolean test = a + b < c || a + c < b || b + c < a;
        boolean first = a == b && b == c;
        boolean second = a == b || a == c || b == c;
        if (test) {
            return NOTATRIANGLE;
        }
        if (first) {
            return EQUILATERAL;
        }
        if (second) {
            return ISOSCELES;
        }
        return VERSATILE;

    }
    // END
}
