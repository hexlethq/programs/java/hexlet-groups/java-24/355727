package exercise;

class App {
    // BEGIN
    public static int[] reverse(int[] inputArray) {
        int lenght = inputArray.length;
        int[] resultArray = new int[lenght];
        for (int i = 0; i < lenght / 2; i++) {
            resultArray[i] = inputArray[inputArray.length - i - 1];
            resultArray[inputArray.length - i - 1] = inputArray[i];
            resultArray[lenght / 2] = inputArray[lenght / 2];

        }
        return resultArray;
    }

    public static int getIndexOfMaxNegative(int[] inputArray) {
        int minimalNumber = Integer.MIN_VALUE;
        int index = -1;
        for (int i = 0; i < inputArray.length; i++) {
            if (inputArray[i] < 0 && inputArray[i] > minimalNumber) {
                minimalNumber = inputArray[i];
                index = i;
            }
        }
        return index;
    }
    // END
}
