package exercise;

import java.util.Map;
import java.util.HashMap;

// BEGIN
public class InMemoryKV implements KeyValueStorage {
    private Map<String, String> keyValueMap = new HashMap<>();

    public InMemoryKV(Map<String, String> initialMap) {
        this.keyValueMap.putAll(initialMap);
    }

    @Override
    public void set(String key, String value) {
        this.keyValueMap.put(key, value);
    }

    @Override
    public void unset(String key) {
        keyValueMap.remove(key);
    }

    @Override
    public String get(String key, String defaultValue) {
        var resultedString = keyValueMap.get(key);
        if (resultedString == null) return defaultValue;
        return resultedString;
    }

    @Override
    public Map<String, String> toMap() {
        return new HashMap<>(this.keyValueMap);
    }
}

// END
