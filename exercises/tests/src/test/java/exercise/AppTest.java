package exercise;

import java.util.Collections;
import java.util.List;
import java.util.Arrays;
import java.util.ArrayList;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;



class AppTest {
    List<Integer> firstTestList = new ArrayList<>(Arrays.asList(1, 2, 3, 4));
    List<Integer> secondTestList = new ArrayList<>(Arrays.asList(7, 3, 10));
    List<Integer> thirdTestList = new ArrayList<>(Collections.emptyList());
    int number1 = 2;
    int number2 = 8;
    int number3 = 3;
    List<Integer> expectedResult1 = new ArrayList<>(Arrays.asList(1, 2));
    List<Integer> expectedResult2 = new ArrayList<>(Arrays.asList(7, 3, 10));

    @Test
    void testTake() {
        // BEGIN
        Assertions.assertEquals(App.take(firstTestList, number1), expectedResult1);
        Assertions.assertEquals(App.take(secondTestList, number2), expectedResult2);
        Assertions.assertTrue(App.take(thirdTestList, number3).isEmpty());
        // END
    }
}
