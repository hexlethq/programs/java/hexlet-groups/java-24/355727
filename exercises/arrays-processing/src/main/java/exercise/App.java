package exercise;

class App {
    // BEGIN
    public static int[] getElementsLessAverage(int[] inputArray) {

        if (inputArray.length == 0) {
            return new int[0];
        }

        int[] resultArray = new int[arraysize(inputArray)];

        for (int i = 0; i < inputArray.length; i++) {
            if (inputArray[i] <= getAverage(inputArray)) {
                resultArray[i] = inputArray[i];
            }
        }
        return resultArray;
    }

    public static int arraysize(int[] inputArray) {
        int arraysize = 0;
        for (int i : inputArray) {
            if (i <= getAverage(inputArray)) {
                arraysize++;
            }
        }
        return arraysize;
    }

    public static int getAverage(int[] inputArray) {
        int sum = 0;
        for (int i : inputArray) {
            sum = sum + i;
        }
        return sum / inputArray.length;
    }

//    Самостоятельная работа

    public static int getSumBeforeMinAndMax(int[] inputArray) {
        if (inputArray.length == 0) {
            return 0;
        }

        int minimum = getValueIndex(inputArray, getMin(inputArray));
        int maximum = getValueIndex(inputArray, getMax(inputArray));
        int sum = 0;
        for (int i = minimum + 1; i < maximum; i++) {
            sum = sum + inputArray[i];
        }
        return sum;
    }

    public static int getMax(int[] inputArray) {
        int max = Integer.MIN_VALUE;
        for (int a : inputArray) {
            max = Math.max(a, max);
        }
        return max;
    }

    public static int getMin(int[] inputArray) {
        int min = Integer.MAX_VALUE;
        for (int a : inputArray) {
            min = Math.min(a, min);
        }
        return min;
    }

    public static int getValueIndex(int[] arr, int value) {
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == value) {
                return i;
            }
        }
        return -1;
    }

    // END
}

