package exercise;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.stream.Collectors;

class App {
    // BEGIN
    public static String buildList(String[] inputArray) {
        if (inputArray.length == 0) {
            return "";
        }
        String HtmlConvert = Arrays.stream(inputArray)
                .map(str -> "  <li>" + str + "</li>\n")
                .collect(Collectors.joining("", "<ul>\n", "</ul>"));
        return HtmlConvert;
    }

    public static String getUsersByYear(String[][] users, int year) {
        String Users = Arrays.stream(users)
                .filter(str -> LocalDate.parse(str[1]).getYear() == year)
                .map(str -> "  <li>" + str[0] + "</li>\n")
                .collect(Collectors.joining("", "<ul>\n", "</ul>"));
        return Users.length() == 10 ? "" : Users;
    }
    // END

    // Это дополнительная задача, которая выполняется по желанию.
    public static String getYoungestUser(String[][] users, String date) throws Exception {
        // BEGIN
        return "";
        // END
    }
}
