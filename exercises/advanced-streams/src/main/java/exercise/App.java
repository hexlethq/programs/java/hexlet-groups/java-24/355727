package exercise;

import java.util.stream.Collectors;
import java.util.Arrays;

// BEGIN
public class App {
    public static String getForwardedVariables(String config) {
        String result;
        try (var stream = Arrays.stream(config.split("\n"))) {
            result = stream
                    .filter(x -> x.startsWith("environment"))
                    .filter(x -> x.contains("X_FORWARDED_"))
                    .map(App::changeVariables)
                    .collect(Collectors.joining(","));
        }
        return result;
    }

    private static String changeVariables(String row) {
        String result;
        try (var stream = Arrays.stream(row.split(","))) {
            result = stream
                    .map(x -> x.replaceAll("environment=", ""))
                    .map(x -> x.replaceAll("\"", ""))
                    .filter(x -> x.startsWith("X_FORWARDED_"))
                    .map(x -> x.replaceAll("X_FORWARDED_", ""))
                    .map(x -> x.replaceAll(",", ""))
                    .map(x -> x.replaceAll(" ", ""))
                    .collect(Collectors.joining(","));
        }
        return result;
    }
}

//END
