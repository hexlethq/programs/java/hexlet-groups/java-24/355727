package exercise;

class App {
    // BEGIN
    public static String getAbbreviation(String text) {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < text.length(); i++) {
            if (i == 0) {
                result.append(text.substring(i, i + 1).toUpperCase());
            } else if (text.charAt(i - 1) == ' ') {
                result.append(text.substring(i, i + 1).toUpperCase());
            }
        }


        return result.toString();
    }
    // END
}
